#
# Makefile for CONVILER

calc: calc.tab.o lex.yy.o
	g++ -o calc lex.yy.o calc.tab.o

calc.tab.cpp calc.tab.hpp: calc.ypp
	bison -d calc.ypp

lex.yy.cpp: calc.lpp
	flex -o lex.yy.cpp calc.lpp

lex.yy.o: lex.yy.cpp calc.tab.hpp
	g++ -c lex.yy.cpp

calc.tab.o: calc.tab.cpp
	g++ -c calc.tab.cpp

.INTERMEDIATE: calc.tab.o lex.yy.o lex.yy.cpp calc.tab.cpp calc.tab.hpp

.PHONY: clean
clean:
	-rm -f *.o *.yy.cpp *.tab.?pp calc
