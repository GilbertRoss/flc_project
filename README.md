# FLC Project - Multi function calculator and converter

### To run: 
* cd to File:

```
cd/FLC_Project
```
* run make:
```
make
```
* run the make output
```
./calc
```

### Basic functions:

```
2+2
3*3
4/4
(1+2+3)*5

```

```
4=binary;
20=octal;
30=hexadecimal;
10=rand;
2=square;
2=fib;

```

